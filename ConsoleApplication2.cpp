#include <stdio.h>
#include <omp.h>
#include <conio.h>
#include <ctime>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif
#include <GL/freeglut.h>
#define REFRESH_DELAY 1
#define WIDTH 1000
#define HEIGHT 1000
#define DEBUG 0
typedef struct
{
	int status;
	int r, g, b;
} Point;
int points[WIDTH * HEIGHT];
int pointsOutput[WIDTH * HEIGHT];
GLubyte PixelBuffer[WIDTH * HEIGHT * 3];
int mouse_buttons = 0;
int mouse_old_y = 0;
int mouse_old_x = 0;
void mouse(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN)
	{
		mouse_buttons |= 1 << button;
	}
	else if (state == GLUT_UP)
	{
		mouse_buttons = 0;
	}
	mouse_old_x = x;
	mouse_old_y = y;
	for (int i = 0; i < 100; i++) {
		int j = 0;
		for (j = 0; j < 100; j++) {
			points[(x + i) * WIDTH + (WIDTH - y + j)] = 255;
		}
	}
}
void timerEvent(int value)
{
	if (glutGetWindow())
	{
		glutPostRedisplay();
		glutTimerFunc(REFRESH_DELAY, timerEvent, 0);
	}
}
void makePixel(int x, int y, int r, int g, int b, GLubyte* pixels, int width, int height)
{
	if (0 <= x && x < width && 0 <= y && y < height) {
		int position = (x + y * width) * 3;
		pixels[position] = b;
		pixels[position + 1] = g;
		pixels[position + 2] = r;
	}
}
void run() {
	int countThreads = 1;
	int offset = (WIDTH * HEIGHT) / countThreads;
	int key = 0;
	int count = 1;
	int dif = count * 4;
	//#pragma omp parallel num_threads(countThreads)
		//{
	int from, to;
	from = omp_get_thread_num() * offset;
	to = from + offset;
	if (to + 1 == (WIDTH * HEIGHT)) {
		to++;
	}
#pragma omp parallel for num_threads(8)
	for (int i = from; i < to; i++) {
		int origin = points[i];
		key = i + 1;
		if (key >= 0 && key < WIDTH * HEIGHT)
			if (points[i] - points[key] > dif) {
				origin -= count;
			}
			else
				if (points[key] - points[i] > dif) {
					origin += count;
				}
		key = i - 1;
		if (key >= 0 && key < WIDTH * HEIGHT)
			if (points[i] - points[key] > dif) {
				origin -= count;
			}
			else
				if (points[key] - points[i] > dif) {
					origin += count;
				}

		key = i + WIDTH;
		if (key >= 0 && key < WIDTH * HEIGHT)
			if (points[i] - points[key] > dif) {
				origin -= count;
			}
			else
				if (points[key] - points[i] > dif) {
					origin += count;
				}

		key = i - WIDTH;
		if (key >= 0 && key < WIDTH * HEIGHT)
			if (points[i] - points[key] > dif) {
				origin -= count;
			}
			else
				if (points[key] - points[i] > dif) {
					origin += count;
				}
		pointsOutput[i] = origin;
		//}
	}
#pragma omp parallel for num_threads(8)
	for (int i = 0; i < WIDTH * HEIGHT; i++) points[i] = pointsOutput[i];
}
void writePoints() {
	int sum = 0;
	if (DEBUG) {
		for (int i = 0; i < WIDTH; i++) {
			for (int j = 0; j < HEIGHT; j++) {
				printf("%d ", points[i * WIDTH + j]);
			}
			printf("\n");
		}
	}
	for (int i = 0; i < WIDTH * HEIGHT; i++) {
		sum += points[i];
	}
	printf("sum=%d \n", sum);
}
unsigned int start_time, time_;
void display()
{
	start_time = clock();
	run();
	time_ = clock() - start_time;
	printf("time=%d\n", time_);
	writePoints();
	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(255, 255, 0.0, 0);
	for (int i = 0; i < WIDTH; i++) {
		for (int j = 0; j < HEIGHT; j++) {
			makePixel(i, j,
				0,
				points[i * WIDTH + j],
				0,
				PixelBuffer, WIDTH, HEIGHT);
		}
	}
	glDrawPixels(WIDTH, WIDTH, GL_RGB, GL_UNSIGNED_BYTE, PixelBuffer);
	glutSwapBuffers();
}
void setXYR(int x, int y, int value) {
	points[x * WIDTH + y] = value;
}
void initGL(int* argc, char** argv)
{
	glutInit(argc, argv);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutCreateWindow("Simulation in openMP, Aleksej");
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutTimerFunc(REFRESH_DELAY, timerEvent, 0);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glLoadIdentity();
	glutMainLoop();
}
int main(int argc, char* argv[])
{
	for (int i = 0; i < WIDTH * HEIGHT; i++)pointsOutput[i] = 0;
	if (DEBUG) {
		setXYR(0, 0, 1);
		setXYR(0, 1, 2);
		setXYR(0, 2, 3);

		setXYR(1, 0, 4);
		setXYR(1, 1, 45);
		setXYR(1, 2, 5);

		setXYR(2, 0, 6);
		setXYR(2, 1, 7);
		setXYR(2, 2, 8);
	}
	writePoints();
	initGL(&argc, argv);
	return 0;
}
